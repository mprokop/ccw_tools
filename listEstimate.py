from oauthlib.oauth2 import LegacyApplicationClient
from requests_oauthlib import OAuth2Session
import requests


pass_file = open("./pass.txt","r") #opens file with name of "pass.txt"
fileList = pass_file.read().splitlines()
client_id = fileList[0]
client_secret = fileList[1]
username= fileList[2]
password = fileList[3]

oauth = OAuth2Session(client=LegacyApplicationClient(client_id=client_id))
token = oauth.fetch_token(token_url='https://cloudsso.cisco.com/as/token.oauth2',
        username=username, password=password, client_id=client_id,
        client_secret=client_secret)

headers = {"Content-Type": "text/xml", "Authorization": "Bearer "+str(token['access_token'])}
body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:hs="https://api.cisco.com/commerce/EST/v2/async/listEstimate">
 <soapenv:Body>
 <GetQuote releaseID="2014" versionID="1.0" systemEnvironmentCode="Production" languageCode="en-US" xmlns="http://www.openapplications.org/oagis/10">
 	 <ApplicationArea>
 	  <Sender>
       <ComponentID schemeAgencyID="Cisco">B2B-3.0</ComponentID>
      </Sender>
 	 </ApplicationArea>
     <DataArea>
		    <Get maxItems="25">
		      <Expression expressionLanguage="SortBy">LAST_MODIFIED</Expression>
		    </Get>
		    <Quote>
		      <QuoteHeader>
		        <Status>
		          <Code typeCode="EstimateStatus">ALL</Code>
		        </Status>
		      </QuoteHeader>
		    </Quote>
	 </DataArea>
</GetQuote>
 </soapenv:Body>
</soapenv:Envelope>"""



out = requests.post("https://api.cisco.com/commerce/EST/v2/async/listEstimate", data=body, headers=headers)

print (out.content)



