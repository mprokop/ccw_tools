from oauthlib.oauth2 import LegacyApplicationClient
from requests_oauthlib import OAuth2Session
from lxml import etree as ET
import requests
import csv



pass_file = open("./pass.txt","r") #opens file with name of "pass.txt"
fileList = pass_file.read().splitlines()
client_id = fileList[0]
client_secret = fileList[1]
username= fileList[2]
password = fileList[3]

SOAP_NS = 'http://schemas.xmlsoap.org/soap/envelope/'
HS_NS = 'https://api.cisco.com/commerce/EST/v2/async/createEstimate'
ns_map = {'soapenv': SOAP_NS, 'hs': HS_NS}


oauth = OAuth2Session(client=LegacyApplicationClient(client_id=client_id))
token = oauth.fetch_token(token_url='https://cloudsso.cisco.com/as/token.oauth2',
        username=username, password=password, client_id=client_id,
        client_secret=client_secret)
headers = {"Content-Type": "text/xml", "Authorization": "Bearer "+str(token['access_token'])}

parentID_No=0
lineNumber_No=1

with open("./configs/edge_48P_40G") as f:
    reader = csv.DictReader(f)
    data = [r for r in reader]

# Build XML/SOAP request

env = ET.Element(ET.QName(SOAP_NS, 'Envelope'), nsmap=ns_map)
body = ET.SubElement(env, ET.QName(SOAP_NS, 'Body'), nsmap=ns_map)
processQuote = ET.SubElement(body,'ProcessQuote', releaseID="2014",versionID="1.0", systemEnvironmentCode="Production", languageCode="en-US", xmlns="http://www.openapplications.org/oagis/10")
applicationArea = ET.SubElement(processQuote,'ApplicationArea')
sender = ET.SubElement(applicationArea,'Sender')
componentID = ET.SubElement(sender,'ComponentID',schemeAgencyID="Cisco")
componentID.text = ET.CDATA('B2B-3.0')
dataArea = ET.SubElement(processQuote,'DataArea')
quote = ET.SubElement(dataArea,'Quote')
quoteHeader = ET.SubElement(quote,'QuoteHeader')
message = ET.SubElement(quoteHeader,'Message')
note = ET.SubElement(message,'Note')
note.text = ET.CDATA("TEST DESC")


# New Item(s) loop

for key in data:
	print (lineNumber_No)
	quoteLine = ET.SubElement(quote,'QuoteLine')
	quoteLineNumber = ET.SubElement(quoteLine,'LineNumberID')
	quoteLineNumber.text = ET.CDATA(str(lineNumber_No))
	item = ET.SubElement(quoteLine,'Item')
	idPid = ET.SubElement(item,'ID',typeCode="PartNumber",schemeAgencyID="Cisco")
	idPid.text = ET.CDATA(str(key['pid']))
	specification = ET.SubElement(item,'Specification')
	propertySpec = ET.SubElement(specification,'Property')
	parentID = ET.SubElement(propertySpec,'ParentID')
	parentID.text = ET.CDATA(str(parentID_No))
	extProperty = ET.SubElement(propertySpec,'Extension')
	valueText = ET.SubElement(extProperty,'ValueText',typeCode="ConfigurationPath")
	valueText.text = ET.CDATA(str(key['configPath']))
	extensionItem = ET.SubElement(item,'Extension')
	quantityExtension = ET.SubElement(extensionItem,'Quantity',unitCode="Each")
	quantityExtension.text = ET.CDATA(key['quantity'])
	parentID_No = 1
	lineNumber_No += 1


# Create XML/SOAP request
xml_string = ET.tostring(env, pretty_print=True)

# print (xml_string)

out = requests.post("https://api.cisco.com/commerce/EST/v2/async/createEstimate", data=xml_string, headers=headers)

print (out.content)
